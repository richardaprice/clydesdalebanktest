//
//  TableViewModelTests.m
//  Cake List
//
//  Created by Richard Price on 25/04/2017.
//  Copyright © 2017 Stewart Hart. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TableViewModel.h"


@interface TestDelegate : NSObject <TableViewModelDelegate>

@property (nonatomic) BOOL dataWasFetched;
@property (nonatomic) BOOL errorWasCreated;

@end

@implementation TestDelegate

-(void ) dataFetched {
    self.dataWasFetched = true;
}

-(void ) errorReturned {
    self.errorWasCreated = true;
}

@end



@interface TableViewModelTests : XCTestCase

@property (nonatomic, strong) TableViewModel *sut;
@property (nonatomic, strong) TestDelegate *testDelegate;

@end

@implementation TableViewModelTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    self.sut = nil;
    self.testDelegate = nil;
    
    [super tearDown];
}

- (void) doSetUpWithJson:(NSString *)json {
    NSString *pathComponent = [NSString stringWithFormat:@"%@.json", json];
    NSString *filePath = [[[NSBundle bundleForClass:[self class]] resourcePath] stringByAppendingPathComponent:pathComponent];
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
    
    self.testDelegate = [TestDelegate new];
    self.sut = [[TableViewModel alloc] initWithDelegate:self.testDelegate url:fileURL];
}

- (void)testObjectsParsedAndDelegateCalled {
    [self doSetUpWithJson:@"cakes"];
    
    XCTAssertEqual(2, self.sut.objects.count);
    
    NSDictionary *firstObject = self.sut.objects[0];
    XCTAssertTrue([@"First cake" isEqualToString:firstObject[@"title"]]);
    XCTAssertTrue([@"First cake description" isEqualToString:firstObject[@"description"]]);
    XCTAssertTrue([@"https://www.google.com" isEqualToString:firstObject[@"image"]]);
    
    NSDictionary *secondObject = self.sut.objects[1];
    XCTAssertTrue([@"Second cake" isEqualToString:secondObject[@"title"]]);
    XCTAssertTrue([@"Second cake description" isEqualToString:secondObject[@"description"]]);
    XCTAssertTrue([@"https://www.bbc.co.uk" isEqualToString:secondObject[@"image"]]);
    
    XCTAssertTrue(self.testDelegate.dataWasFetched);
}

- (void)testObjectsParsedButNotDictionaryErrorReturned {
    [self doSetUpWithJson:@"noCakes"];
    
    XCTAssertNil(self.sut.objects);
    
    XCTAssertTrue(self.testDelegate.errorWasCreated);
}

- (void)testObjectsNotParsedErrorReturned {
    [self doSetUpWithJson:@"incorrectJson"];
    
    NSString *filePath = [[[NSBundle bundleForClass:[self class]] resourcePath] stringByAppendingPathComponent:@"incorrectJson.json"];
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
    
    self.testDelegate = [TestDelegate new];
    self.sut = [[TableViewModel alloc] initWithDelegate:self.testDelegate url:fileURL];
    
    XCTAssertNil(self.sut.objects);
    
    XCTAssertTrue(self.testDelegate.errorWasCreated);
}

- (void)testNoDataErrorReturned {
    NSString *filePath = [[[NSBundle bundleForClass:[self class]] resourcePath] stringByAppendingPathComponent:@"noData.json"];
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
    
    self.testDelegate = [TestDelegate new];
    self.sut = [[TableViewModel alloc] initWithDelegate:self.testDelegate url:fileURL];
    
    XCTAssertNil(self.sut.objects);
    
    XCTAssertTrue(self.testDelegate.errorWasCreated);
}

@end
