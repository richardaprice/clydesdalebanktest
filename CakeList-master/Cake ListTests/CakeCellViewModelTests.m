//
//  CakeCellViewModelTests.m
//  Cake List
//
//  Created by Richard Price on 25/04/2017.
//  Copyright © 2017 Stewart Hart. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CakeCellViewModel.h"

@interface CakeCellViewModelTests : XCTestCase

@end

@implementation CakeCellViewModelTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testCorrectDictionary {
    NSDictionary *inputDictionary = @{@"title":@"A title", @"desc": @"A description", @"image": @"An image address"};
    
    CakeCellViewModel *sut = [[CakeCellViewModel alloc] initWithDictionary:inputDictionary];
    
    XCTAssertTrue([sut.title isEqualToString:@"A title"]);
    XCTAssertTrue([sut.cakeDescription isEqualToString:@"A description"]);
    XCTAssertTrue([sut.imageAddress isEqualToString:@"An image address"]);
}

- (void)testInCorrectDictionary {
    NSDictionary *inputDictionary = @{@"a title":@"A title", @"a desc": @"A description", @"a image": @"An image address"};
    
    CakeCellViewModel *sut = [[CakeCellViewModel alloc] initWithDictionary:inputDictionary];
    
    XCTAssertNil(sut.title);
    XCTAssertNil(sut.cakeDescription);
    XCTAssertNil(sut.imageAddress);
}

- (void)testNilDictionary {
    CakeCellViewModel *sut = [[CakeCellViewModel alloc] initWithDictionary:nil];
    
    XCTAssertNil(sut.title);
    XCTAssertNil(sut.cakeDescription);
    XCTAssertNil(sut.imageAddress);
}

@end
