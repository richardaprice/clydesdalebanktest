//
//  CakeCellViewModel.h
//  Cake List
//
//  Created by Richard Price on 25/04/2017.
//  Copyright © 2017 Stewart Hart. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CakeCellViewModel : NSObject

@property (nonatomic, strong, readonly) NSString *title;
@property (nonatomic, strong, readonly) NSString *cakeDescription;
@property (nonatomic, strong, readonly) NSString *imageAddress;

-(instancetype)initWithDictionary:(NSDictionary *)dict;

@end
