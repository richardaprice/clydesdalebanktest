//
//  TableViewModel.h
//  Cake List
//
//  Created by Richard Price on 25/04/2017.
//  Copyright © 2017 Stewart Hart. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TableViewModelDelegate <NSObject>

-(void ) dataFetched;
-(void ) errorReturned;

@end

@interface TableViewModel : NSObject

@property (strong, nonatomic, readonly) NSArray *objects;

-(void) getData;

- (instancetype)initWithDelegate: (id <TableViewModelDelegate>) delegate url:(NSURL *)url;

@end
