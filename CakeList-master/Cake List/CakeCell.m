//
//  CakeCell.m
//  Cake List
//
//  Created by Stewart Hart on 19/05/2015.
//  Copyright (c) 2015 Stewart Hart. All rights reserved.
//

#import "CakeCell.h"
#import "CakeCellViewModel.h"

@implementation CakeCell

-(void) setUpWithCellViewModel:(CakeCellViewModel *)cellViewModel {
    self.titleLabel.text = cellViewModel.title;
    self.descriptionLabel.text = cellViewModel.cakeDescription;
    
    NSURL *aURL = [NSURL URLWithString:cellViewModel.imageAddress];
    NSData *data = [NSData dataWithContentsOfURL:aURL];
    UIImage *image = [UIImage imageWithData:data];
    [self.cakeImageView setImage:image];
}

@end
