//
//  CakeCellViewModel.m
//  Cake List
//
//  Created by Richard Price on 25/04/2017.
//  Copyright © 2017 Stewart Hart. All rights reserved.
//

#import "CakeCellViewModel.h"

@interface CakeCellViewModel ()

@property (nonatomic, strong, readwrite) NSString *title;
@property (nonatomic, strong, readwrite) NSString *cakeDescription;
@property (nonatomic, strong, readwrite) NSString *imageAddress;

@end

@implementation CakeCellViewModel

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.title = dict[@"title"];
        self.cakeDescription = dict[@"desc"];
        self.imageAddress = dict[@"image"];
    }
    return self;
}

@end
