//
//  TableViewModel.m
//  Cake List
//
//  Created by Richard Price on 25/04/2017.
//  Copyright © 2017 Stewart Hart. All rights reserved.
//

#import "TableViewModel.h"

@interface TableViewModel ()

@property (strong, nonatomic, readwrite) NSArray *objects;
@property (weak, nonatomic) id <TableViewModelDelegate> delegate;
@property (strong,nonatomic) NSURL *url;

@end

@implementation TableViewModel

- (instancetype)initWithDelegate: (id <TableViewModelDelegate>) delegate url:(NSURL *)url;
{
    self = [super init];
    if (self) {
        self.delegate = delegate;
        self.url = url;
        [self getData];
    }
    return self;
}

-(void) getData {
    
    NSData *data = [NSData dataWithContentsOfURL:self.url];
    
    NSError *jsonError;
    if (data) {
        id responseData = [NSJSONSerialization
                           JSONObjectWithData:data
                           options:kNilOptions
                           error:&jsonError];
        
        if (!jsonError && [responseData isKindOfClass:[NSArray class]]){
            NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:responseData];
            NSArray *arrayWithoutDuplicates = [orderedSet array];
            self.objects = arrayWithoutDuplicates;
            [self.delegate dataFetched];
            return;
        }
    }
    [self.delegate errorReturned];
}

@end
